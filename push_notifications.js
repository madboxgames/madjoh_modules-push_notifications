define(function(){
	var PushNotification = {
		appKey 		: null,
		projectId 	: null,

		init : function(settings, appKey, projectId, callback){
			PushNotification.settings 	= settings;
			PushNotification.appKey 	= appKey;
			PushNotification.projectId 	= projectId;
			document.addEventListener('deviceready', function(){PushNotification.onReady(callback);}, false);
			document.addEventListener('push-notification', PushNotification.onReceived, false);
		},

		onReady : function(callback){
			if(!window.plugins || !window.plugins.OrtcPushPlugin) return;

			window.plugins.OrtcPushPlugin.checkForNotifications(PushNotification.onReceived);
			callback();
		},

		// CONNECT
			connect : function(channel, token, callback){
				if(!window.plugins || !window.plugins.OrtcPushPlugin) return;

				var OrtcPushPlugin = window.plugins.OrtcPushPlugin;
					OrtcPushPlugin.log('ORTC Connecting');

				var params = {
					appKey 		: PushNotification.appKey,
					projectId 	: PushNotification.projectId,
					channel 	: channel,
					token 		: token
				};
				OrtcPushPlugin.log(JSON.stringify(params));

				var onConnectSuccess = function(data){
					document.removeEventListener('onException', onConnectError);

					OrtcPushPlugin.log('ORTC Connected');
					if(data) OrtcPushPlugin.log(JSON.stringify(data));
					
					PushNotification.subscribe(channel, callback);
				};
				var onConnectError = function(error){
					document.removeEventListener('onException', onConnectError);
					OrtcPushPlugin.log('ORTC Connection Error');

					if(error){
						OrtcPushPlugin.log(JSON.stringify(error));

						if(error.description === 'Already connected') onConnectSuccess();
					}
				};

				document.addEventListener('onException', onConnectError, false);
				OrtcPushPlugin.connect(
					{
						'appkey'	: PushNotification.appKey,
						'token'		: token,
						'url'		: 'https://ortc-developers.realtime.co/server/ssl/2.1/',
						'metadata' 	: 'my_metadata',
						'projectId' : PushNotification.projectId
					},
					onConnectSuccess
				);
			},

		// SUBSCRIBE
			subscribe : function(channel, callback){
				if(!window.plugins || !window.plugins.OrtcPushPlugin) return;

				var OrtcPushPlugin = window.plugins.OrtcPushPlugin;
					OrtcPushPlugin.log('ORTC Subscribing : ' + channel);

				var onSubscribeSuccess = function(data){
					document.removeEventListener('onException', onSubscribeError);

					OrtcPushPlugin.log('ORTC Subscribed : ' + channel);
					if(callback) callback(data);
				};
				var onSubscribeError = function(error){
					document.removeEventListener('onException', onSubscribeError);
					OrtcPushPlugin.log('ORTC Subscription Error');

					if(error) OrtcPushPlugin.log(JSON.stringify(error));
				};

				document.addEventListener('onException', onSubscribeError, false);
				OrtcPushPlugin.subscribe({channel : channel}, onSubscribeSuccess);
			},

		// UNSUBSCRIBE
			unsubscribe : function(channel, token, callback){
				if(!window.plugins || !window.plugins.OrtcPushPlugin) return;

				var OrtcPushPlugin = window.plugins.OrtcPushPlugin;
					OrtcPushPlugin.log('ORTC Subscribing before Unsubscribing : ' + channel);

				var doUnsubscribe = function(){
					PushNotification.doUnsubscribe(channel, callback);
				};

				// Need to make sure you are connected before unsubscribing
				PushNotification.connect(channel, token, doUnsubscribe);
			},
			doUnsubscribe : function(channel, callback){
				var OrtcPushPlugin = window.plugins.OrtcPushPlugin;
					OrtcPushPlugin.log('ORTC Unsubscribing : ' + channel);

				var onUnsubscribeSuccess = function(data){
					document.removeEventListener('onException', onUnsubscribeError);

					OrtcPushPlugin.log('ORTC Unsubscribed : ' + channel);
					if(callback) callback();
				};
				var onUnsubscribeError = function(error){
					document.removeEventListener('onException', onUnsubscribeError);
					OrtcPushPlugin.log('ORTC Unsubscribe Error');
					
					if(error) OrtcPushPlugin.log(JSON.stringify(error));
				};

				document.addEventListener('onException', onUnsubscribeError, false);
				OrtcPushPlugin.unsubscribe({channel : channel}, onUnsubscribeSuccess);
			},

		// DISCONNECT
			disconnect : function(){
				if(!window.plugins || !window.plugins.OrtcPushPlugin) return;

				var OrtcPushPlugin = window.plugins.OrtcPushPlugin;
					OrtcPushPlugin.log('ORTC Disconnecting');

				var onDisconnectSuccess = function(data){
					document.removeEventListener('onException', onDisconnectError);

					OrtcPushPlugin.log('ORTC Disconnected');
				};
				var onDisconnectError = function(error){
					document.removeEventListener('onException', onDisconnectError);
					OrtcPushPlugin.log('ORTC Disconnect Error');

					if(error) OrtcPushPlugin.log(JSON.stringify(error));
				};

				document.addEventListener('onException', onDisconnectError, false);
				OrtcPushPlugin.disconnect(onDisconnectSuccess);

				PushNotification.setBadgeNumber(0);
			},

		// HANDLE NOTIFICATIONS
			onReceived : function(notification){
				var OrtcPushPlugin = window.plugins.OrtcPushPlugin;
					OrtcPushPlugin.log(JSON.stringify(notification));
					PushNotification.settings.handle(notification);
					PushNotification.clearNotifications();
			},
			setBadgeNumber : function(nb){
				if(!window.plugins || !window.plugins.OrtcPushPlugin) return;

				var OrtcPushPlugin = window.plugins.OrtcPushPlugin;
				OrtcPushPlugin.setApplicationIconBadgeNumber(nb, function(){});

				if(nb === 0) PushNotification.clearNotifications();
			},
			clearNotifications : function(){
				if(!window.cordova || !window.cordova.plugins.notification) return;

				cordova.plugins.notification.local.clearAll();
			}
	};

	return PushNotification;
});